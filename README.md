# Android-UI-Testing-Espresso

This simple Android application was created for the purpose of building an automated testing framework as part of the course Methods and Techniques of Software Testing.

 Espresso framework was used to test the user interface, and code optimization was done with the help of Screen Robot.
 
 ## Table of contents
* [About the app](#about-the-app)
* [A brief intrp](#a-brief-intro)
* [Setting up the environment](#setting-up-the-environment)
* [Test optimization](#test-optimization)
* [Test execution](#test-execution)
 
### About the app
***
This application allows you to enter a shopping limit, and display and select items whose price is within that limit. When an item is selected, its price is added to the sum, and other items are allowed to be selected according to the rest. 

<img src="/uploads/76a07d905007dfa23d3744b9aba102ec/ezgif.com-gif-maker.gif" width="200" height="480">

### A brief intro
***
Manual application testing is time consuming and often tedious, especially when it comes to larger scale applications. Espresso allows you to write automated tests that run relatively quickly and come in handy in cases where you need to go through the entire application after adding a new feature or functionality. Although it can be used for black-box testing, Espresso shows its full power in the hands of developers, ie people who are familiar with the codebase of tests.

### Setting up the environment
***
All the code needed to run the application is located within MainActivity in the hr.ferit.shopsmart package, and is written in the Kotlin programming language. In the package of the same name, but marked androidTest, there is a class that contains tests for testing the user interface of the application.


<img src="/uploads/8fb676c53435cb53c7784c8b1c78bc45/slika_1.png" width="380" height="200">


There is no need to include anything to write UI tests with the Espresso framework, because every newly created Android project, by default, has some test dependencies included, which among others is the Espresso library.

```sh
androidTestImplementation 'androidx.test.ext:junit:1.1.2'
androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
```

To prevent writing boilerplate code it is good to add some more auxiliary libraries to determine the rules and the truth of the expression. The following Google libraries are added to app / build.gradle:

```sh
androidTestImplementation 'androidx.test:rules:1.3.0'
androidTestImplementation 'androidx.test.ext:truth:1.3.0'
androidTestImplementation 'androidx.test.ext:junit:1.1.2'
```
After adding them, it is necessary to run the sync Gradle file, then you are rady to write your tests.

> Android Studio offers the "Record Espresso Test" option to automatically  create Espresso tests by clicking on the application, however this method  results in unstable and difficult to read tests.
That is why it is much better to write tests manually, and with the help of Screen Robots to optimize them as much as possible.


### Test examples
***
Each test method within the test class is indicated by the @Test annotation. The simplest test is an application launch test that is run using ActivityScenario.

```sh
ActivityScenario.launch(MainActivity::class.java)
```

In each subsequent test method, you need to launch the application first, and then write the code to check what is happening on the screen.

The three main classes that Espresso offers that you need to know in order to write tests are:
* **ViewMatchers** - contains methods that Espresso uses to find the view on your screen with which it needs to interact
* **ViewActions** - contains methods that tell Espresso how to automate your UI
* **ViewAssertions** - contains methods used to check if a view matches a certain set of conditions.

You can find all instances of these classes on this useful [cheat-sheet](https://developer.android.com/training/testing/espresso/cheat-sheet).

When all this is applied the test looks like this:


<img src="/uploads/70b8c1081dace7abf6545b7adf71c3e5/Method_before_use_od_screen_robots.png" width="780" height="380">


### Test optimization
Test writing can be repetitive and you can pull a lot of this duplication out to reuse. One great way to do this is by using Screen Robots.
Screen Robots pull out some of these tasks into helper functions. This makes your tests more expressive, readable and easier to update.

The above test refactored to use Screen Robot now looks like this:


<img src="/uploads/60cc1f296e66161e0290964013f5f55e/method_after_use_of_screen_robots.png" width="780" height="380">

### Test execution
***

**Locally**

Tests can be run from Android Studio on the Emulator or the actual device. The test results are listed in a run tab window.


<img src="/uploads/f16b8c419b57f3a1f30fce294a713df6/slika_android_lokalno.png" width="650" height="380">



**Remotely**

To run Espresso Tests on Firebase TestLabs you will need a Firebase account with set project. Next step is to edit run configurations in Android studio like this:


<img src="/uploads/227d5644883bd07dfa0d0852b7507bcf/edit_configuration.png" width="650" height="380">


After running the tests, the results are recorded on the Firebase TestLabs screen:


<img src="/uploads/967a34ac46e611214a3c272978909843/fireabasetestlab.png" width="780" height="380">



